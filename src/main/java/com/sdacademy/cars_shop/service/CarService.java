package com.sdacademy.cars_shop.service;


import com.sdacademy.cars_shop.dto.CarDto;
import com.sdacademy.cars_shop.entity.Car;
import com.sdacademy.cars_shop.exceptions.CarDoesNotExistException;
import com.sdacademy.cars_shop.exceptions.DuplicateCarException;
import com.sdacademy.cars_shop.repository.CarRepository;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static lombok.AccessLevel.PRIVATE;

@Service
@Slf4j
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
public class CarService {
    CarRepository carRepository;
    ModelMapper modelMapper;

    public List<CarDto> getAll() {
        log.debug("Fetching all cars");
        return carRepository.findAll()
                .stream()
                .map(car -> modelMapper.map(car, CarDto.class))
                .collect(toList());
    }

    public void createCar(final CarDto carDto) {
        val brand = carDto.getBrand();
        val model = carDto.getModel();
        if (carRepository.existsByBrandAndModel(brand, model)) {
            log.error("Car with brand and model already exists");
            throw new DuplicateCarException("Duplicate car");
        }
        val id = carRepository.save(Car.builder()
                .brand(brand)
                .model(model)
                .kilometers(carDto.getKilometers())
                .productionYear(carDto.getProductionYear())
                .build()
        ).getId();
        log.info("Car with id {} has been saved.", id);
    }

    public void updateCar(final CarDto carDto) {
        val id = carDto.getId();
        isCarExistentById(id);
        carRepository.save(Car.builder()
                .id(id)
                .brand(carDto.getBrand())
                .model(carDto.getModel())
                .kilometers(carDto.getKilometers())
                .productionYear(carDto.getProductionYear())
                .build()
        );
        log.info("The car with id {} was updated", id);
    }

    public void deleteCar(final int id){
        isCarExistentById(id);
        carRepository.deleteById(id);
        log.info("Car with id {} has been deleted", id);
    }

    private void isCarExistentById(int id) {
        if (!carRepository.existsById(id)) {
            log.error("The car with id {} doesn't exist", id);
            throw new CarDoesNotExistException("Non existent id");
        }
    }
}
