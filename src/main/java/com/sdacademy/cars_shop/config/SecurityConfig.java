package com.sdacademy.cars_shop.config;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .csrf().ignoringAntMatchers("/api/**")
                .and()
                .authorizeRequests()
                .antMatchers(GET, "/api/cars").hasAnyRole("ADMIN", "CARS")
                .antMatchers(POST, "/api/cars").authenticated()
                .antMatchers("/api/users/").hasRole("USER_ADMIN")
                .anyRequest().permitAll()
                .and()
                .httpBasic()
                .and()
                .formLogin()
                .and()
                .logout();

    }

    @Override
    protected void configure(final AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.inMemoryAuthentication()
                .withUser("admin").password("{noop}Secret_123").roles("ADMIN", "CARS")
                .and()
                .withUser("admin2").password("{noop}Secret_123").roles("USER_ADMIN")
                .and()
                .withUser("admin3").password("{noop}Secret_123").roles("CARS");


    }
}

