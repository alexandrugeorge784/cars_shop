package com.sdacademy.cars_shop.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor
@Getter
@FieldDefaults(level = PRIVATE, makeFinal = true)
public final class ExceptionDto {
    String message;
    String type;
}
