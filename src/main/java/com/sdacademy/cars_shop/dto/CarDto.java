package com.sdacademy.cars_shop.dto;

import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import static lombok.AccessLevel.PRIVATE;

@Data
@FieldDefaults(level = PRIVATE)
public final class CarDto {
    int id;
    @NotBlank(message = "Brand must be provided")
    String brand;
    @NotNull(message = "Number of the kilometers must be provided")
    @Positive(message = "Number of kilometers must be positive")
    int kilometers;
    @NotBlank(message = "Model must be provided")
    String model;
    @NotNull(message = "Production year must be provided")
    @Size(message = "Production year must be greater than 2010 and before current year", min = 2010 , max = 2023)
    int productionYear;
}
