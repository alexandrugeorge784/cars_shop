package com.sdacademy.cars_shop.controller;

import com.sdacademy.cars_shop.dto.CarDto;
import com.sdacademy.cars_shop.dto.ExceptionDto;
import com.sdacademy.cars_shop.exceptions.CarDoesNotExistException;
import com.sdacademy.cars_shop.exceptions.DuplicateCarException;
import com.sdacademy.cars_shop.exceptions.SdException;
import com.sdacademy.cars_shop.service.CarService;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/cars")
public class CarController {
    private final CarService carService;

    @GetMapping
    List<CarDto> getAll() {
        return carService.getAll();
    }

    @PostMapping
    @ResponseStatus(CREATED)
    void createCar(@Valid @RequestBody final CarDto carDto) {
        carService.createCar(carDto);
    }

    @PutMapping
    @ResponseStatus(NO_CONTENT)
    void updateCar(@Valid @RequestBody final CarDto carDto) {
        carService.updateCar(carDto);
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(OK)
    void deleteCar(@PathVariable final int id) {
        carService.deleteCar(id);
    }

    @ExceptionHandler({CarDoesNotExistException.class, DuplicateCarException.class})
    public ResponseEntity<Object> handleDataBaseError(SdException e){
        val exceptionDto = new ExceptionDto(e.getMessage(), e.getClass().toString());
        return new ResponseEntity<>(exceptionDto, e.getStatus());
    }



}
