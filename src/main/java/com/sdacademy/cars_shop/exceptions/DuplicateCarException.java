package com.sdacademy.cars_shop.exceptions;

public final class DuplicateCarException extends SdException {
    private static final long serialVersionUID = -3335315696480992912L;

    public DuplicateCarException(final String message) {
        super(message);
    }
}
