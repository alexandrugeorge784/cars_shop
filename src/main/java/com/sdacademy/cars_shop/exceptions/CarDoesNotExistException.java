package com.sdacademy.cars_shop.exceptions;

public final class CarDoesNotExistException extends SdException {
    private static final long serialVersionUID = -3335315696480992913L;

    public CarDoesNotExistException(final String message) {
        super(message);
    }
}
