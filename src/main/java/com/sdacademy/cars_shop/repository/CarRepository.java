package com.sdacademy.cars_shop.repository;

import com.sdacademy.cars_shop.entity.Car;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CarRepository extends CrudRepository<Car, Integer> {
    List<Car> findAll();

    boolean existsByBrandAndModel(final String brand, final String model);
}
